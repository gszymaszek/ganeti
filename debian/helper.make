#

include Makefile

install-tests:
	rm -f debian/ganeti-testsuite.install
	for file in $(python_tests) $(python_test_support) $(TEST_FILES) $(qa_scripts); do \
		echo $$file usr/share/ganeti/testsuite/$$(dirname $$file) >> debian/ganeti-testsuite.install; \
	 done
